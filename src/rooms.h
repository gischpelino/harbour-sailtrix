#ifndef ROOMS_H
#define ROOMS_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QFile>
#include <QUrl>
#include <olm/olm.h>
#include "roomsmodel.h"

class Rooms : public QObject
{
    Q_OBJECT
    Q_PROPERTY(RoomsModel* rooms READ rooms WRITE setRooms NOTIFY roomsChanged )
    Q_PROPERTY(bool done READ done NOTIFY done_chg )

public:
    Rooms();
    ~Rooms();
    Q_INVOKABLE bool load();
    Q_INVOKABLE void leave(QString room_id);
    RoomsModel* rooms();
    void setRooms(RoomsModel* roomsModel);
    Q_INVOKABLE void pause();
    Q_INVOKABLE void resume();
    Q_INVOKABLE bool loaded();
    bool done();

signals:
    void roomsChanged();
    void avatarChanged();
    void done_chg();
private:
    QNetworkAccessManager* manager;
    QNetworkAccessManager* imageDownloader;
    QNetworkAccessManager* filterMaker;
    QNetworkAccessManager* leaver;
    QNetworkAccessManager* key_uploader;
    RoomsModel* m_rooms;
    QJsonObject* m_master_obj;
    QJsonObject* olm_sessions;
    QJsonObject* megolm_sessions;
    QNetworkReply *m_reply;
    QString next_batch;
    QString access_token;
    QString user_id;
    QString hs_url;
    QString filter_id;
    QString device_id;
    QString m_curve25519_id;
    OlmAccount* account;
    int num_keys_on_server;
    void loadCache(QJsonDocument doc);
    QString get_filter_json();
    void trigger_filter();
    void write_cache();
    void maintain_key_count();
    bool is_paused;
    bool is_loaded;
    bool load_finished;
    unsigned int* message_index;
    bool get_all;
private slots:
    void handleRooms(QNetworkReply* reply);
    void downloadRoomAvatar(QNetworkReply* reply);
    void handleFilter(QNetworkReply* reply);
    void uploadKeysDone(QNetworkReply* reply);
    QUrl getUrl();
};

#endif // ROOMS_H
