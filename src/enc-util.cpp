#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include <QFile>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonArray>
#include <QDebug>
#include <olm/olm.hh>
#include <QUuid>
#include "enc-util.h"

/* Memory allocation checked by: @HengYeDev */

QString bad_encrypt_str(QString sender, QString reason = QString("Unknown reason")) {
    return QString("{ \"type\": \"m.room.message\", \"content\": { \"msgtype\": \"m.bad_encrypt\", \"body\": \"⚠️ Unable to decrypt: " + reason + "\" }, \"sender\": \"" + sender + "\" }");
}

bool check_olm(QString decrypted, QString sender_user_id, QString m_user_id, QString my_real_key) {
    QJsonDocument doc = QJsonDocument::fromJson(decrypted.toUtf8());
    if (sender_user_id != doc.object().value("sender").toString()
            || m_user_id != doc.object().value("recipient").toString()
            || my_real_key != doc.object().value("recipient_keys").toObject().value("ed25519").toString()) {
        return false;
    }

    return true;
}

bool check_olm_no_verify(QString decrypted, QString sender_user_id, QString m_user_id) {
    QJsonDocument doc = QJsonDocument::fromJson(decrypted.toUtf8());
    if (sender_user_id != doc.object().value("sender").toString()
            || m_user_id != doc.object().value("recipient").toString()) {
        return false;
    }

    return true;
}

QString decrypt(QJsonValue encrypted, QString algorithm, QString sender_key, QString device_id, QString session_id, QString m_curve25519_id, QJsonObject* olm_sessions, QJsonObject* megolm_sessions, QString m_user_id, OlmAccount* account, QString sender, unsigned int* message_index) {
    if (algorithm == QString("m.olm.v1.curve25519-aes-sha2")) {
        QJsonObject obj = encrypted.toObject();
        QJsonObject mapped = obj.value(m_curve25519_id).toObject();
        int type = mapped.value("type").toInt();
        QString body = mapped.value("body").toString();

        if (type == 0) {
            qDebug() << "Type 0";
            if (olm_sessions->contains(sender_key)) {
                int at = 0;
                for (QJsonValue value : olm_sessions->value(sender_key).toObject().value("sessions").toArray()) {
                    QString pickled_session = value.toString();
                    OlmSession* session = (OlmSession*) malloc(olm_session_size());

                    if (session == nullptr) {
                        return bad_encrypt_str(sender, "Cannot allocate memory");
                    }

                    session = olm_session(session);
                    size_t status = olm_unpickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickled_session.toUtf8().data(), pickled_session.toUtf8().length());

                    if (status != olm_error()) {
                        size_t is_this_session = olm_matches_inbound_session(session, body.toUtf8().data(), body.toUtf8().length());
                        if (is_this_session == 1) {
                            size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                            if (decrypted_buffer_length == olm_error()) {
                                qWarning() << olm_session_last_error(session);
                                continue;
                            }
                            char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                            if (decrypted_buffer == nullptr) {
                                return bad_encrypt_str(sender, "Cannot decrypt: cannot allocate memory");
                            }
                            size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                            if (str_len == olm_error()) {
                                qWarning() << olm_session_last_error(session);
                                continue;
                            }
                            // save the session
                            char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                            if (session_pickle == nullptr) {
                                return bad_encrypt_str(sender, "Cannot pickle: cannot allocate memory");
                            }

                            size_t pickle_length = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));


                            QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                            arr.insert(at, QString::fromUtf8(session_pickle, pickle_length));

                            QJsonObject obj = olm_sessions->value(sender_key).toObject();
                            obj.insert("sessions", arr);

                            olm_sessions->insert(sender_key, obj);

                            QString decrypted_str = QString::fromUtf8(decrypted_buffer, str_len);

                            free(session);
                            free(session_pickle);
                            free(decrypted_buffer);

                            return decrypted_str;
                        } else {
                            qDebug() << olm_session_last_error(session);
                        }

                        at++;
                    } else {
                        free(session);
                        return bad_encrypt_str(sender, "Cannot unpickle session");
                    }

                }

                OlmSession* session = (OlmSession*) malloc(olm_session_size());

                if (session == nullptr) {
                    return bad_encrypt_str(sender, "Cannot allocate memory");
                }

                session = olm_session(session);
                size_t result = olm_create_inbound_session_from(session, account, sender_key.toUtf8().data(), sender_key.toUtf8().length(), body.toUtf8().data(), body.toUtf8().length());
                if (result != olm_error()) {
                    size_t result = olm_remove_one_time_keys(account, session);
                    if (result != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            return bad_encrypt_str(sender, olm_session_last_error(session));
                        }


                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            return bad_encrypt_str(sender, "Cannot allocate memory for session pickle");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.append(QString::fromUtf8(session_pickle, pickle_len));
                        QJsonObject sessions_obj = olm_sessions->value(sender_key).toObject();
                        sessions_obj.insert("sessions", arr);
                        olm_sessions->insert(sender_key, sessions_obj);

                        QString decrypted_str = QString::fromUtf8(decrypted_buffer, str_len);

                        free(session);
                        free(session_pickle);
                        free(decrypted_buffer);

                        return decrypted_str;
                    } else {
                        QString session_error = olm_account_last_error(account);
                        free(session);
                        return bad_encrypt_str(sender, session_error);
                    }
                } else {
                    QString session_error = olm_session_last_error(session);
                    free(session);
                    return bad_encrypt_str(sender, session_error);
                }

                free(session);
                return bad_encrypt_str(sender);

            } else {
                OlmSession* session = (OlmSession*) malloc(olm_session_size());

                if (session == nullptr) {
                    return bad_encrypt_str(sender, "cannot allocate memory for session");
                }

                session = olm_session(session);
                size_t result = olm_create_inbound_session_from(session, account, sender_key.toUtf8().data(), sender_key.toUtf8().length(), body.toUtf8().data(), body.toUtf8().length());
                if (result != olm_error()) {
                    size_t result = olm_remove_one_time_keys(account, session);
                    if (result != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 0, body.toUtf8().data(), body.toUtf8().length());
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 0, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            QString err = olm_session_last_error(session);
                            free(session);
                            return bad_encrypt_str(sender, err);
                        }

                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.append(QString::fromUtf8(session_pickle, pickle_len));
                        QJsonObject sessions_obj = olm_sessions->value(sender_key).toObject();
                        sessions_obj.insert("sessions", arr);
                        olm_sessions->insert(sender_key, sessions_obj);

                        QString d_str = QString::fromUtf8(decrypted_buffer, str_len);

                        free(session_pickle);
                        free(session);
                        free(decrypted_buffer);

                        return d_str;
                    } else {
                        QString error = olm_account_last_error(account);
                        free(session);
                        return bad_encrypt_str(sender, error);
                    }
                } else {
                    QString error = olm_session_last_error(session);
                    free(session);
                    return bad_encrypt_str(sender, error);
                }
            }
        } else if (type == 1) {
            if (olm_sessions->contains(sender_key)) {
                int at = 0;
                for (QJsonValue value : olm_sessions->value(sender_key).toObject().value("sessions").toArray()) {
                    QString pickled_session = value.toString();
                    OlmSession* session = (OlmSession*) malloc(olm_session_size());

                    if (session == nullptr) {
                        return bad_encrypt_str(sender, "Cannot allocate memory");
                    }

                    session = olm_session(session);
                    size_t status = olm_unpickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), pickled_session.toUtf8().data(), pickled_session.toUtf8().length());
                    if (status != olm_error()) {
                        size_t decrypted_buffer_length = olm_decrypt_max_plaintext_length(session, 1, body.toUtf8().data(), body.toUtf8().length());

                        if (decrypted_buffer_length == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            free(session);
                            continue;
                        }
                        char* decrypted_buffer = (char*) malloc(decrypted_buffer_length);

                        if (decrypted_buffer == nullptr) {
                            free(session);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t str_len = olm_decrypt(session, 1, body.toUtf8().data(), body.toUtf8().length(), decrypted_buffer, decrypted_buffer_length);

                        if (str_len == olm_error()) {
                            qWarning() << olm_session_last_error(session);
                            free(decrypted_buffer);
                            free(session);
                            continue;
                        }

                        // save the session
                        char* session_pickle = (char*) malloc(olm_pickle_session_length(session));

                        if (session_pickle == nullptr) {
                            free(session);
                            free(decrypted_buffer);
                            return bad_encrypt_str(sender, "Cannot allocate memory");
                        }

                        size_t pickle_len = olm_pickle_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_pickle, olm_pickle_session_length(session));

                        QJsonArray arr = olm_sessions->value(sender_key).toObject().value("sessions").toArray();
                        arr.insert(at, QString::fromUtf8(session_pickle, pickle_len));

                        QJsonObject obj = olm_sessions->value(sender_key).toObject();
                        obj.insert("sessions", arr);


                        QString decrypted = QString::fromUtf8(decrypted_buffer, str_len);
                        olm_sessions->insert(sender_key, obj);
                        free(session);
                        free(session_pickle);
                        free(decrypted_buffer);

                        return decrypted;
                    }
                    at++;
                }


                return bad_encrypt_str(sender, "Cannot find olm session");
            }

            return bad_encrypt_str(sender, "Cannot find olm session");
        }
    } else if (algorithm == QString("m.megolm.v1.aes-sha2")) {
        if (megolm_sessions->contains(session_id)) {
            QJsonObject session_obj = megolm_sessions->value(session_id).toObject();
            // TODO verify sender_key

            OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());

            if (session == nullptr) {
                return bad_encrypt_str(sender, "Cannot allocate session memory");
            }
            session = olm_inbound_group_session(session);
            size_t pickle_status = olm_unpickle_inbound_group_session(session, m_user_id.toUtf8().data(), m_user_id.toUtf8().length(), session_obj.value("pickle").toString().toUtf8().data(), session_obj.value("pickle").toString().toUtf8().length());

            if (pickle_status != olm_error()) {
                int size_of_decrypted = olm_group_decrypt_max_plaintext_length(session, (unsigned char*) encrypted.toString().toUtf8().data(), encrypted.toString().toUtf8().length());
                unsigned char* decrypted_arr = (unsigned char*) malloc(size_of_decrypted);

                if (decrypted_arr == nullptr) {
                    free(session);
                    return bad_encrypt_str(sender, "Cannot allocate memory");
                }
                size_t length_of_decrypted = olm_group_decrypt(session,(unsigned char*) encrypted.toString().toUtf8().data(), encrypted.toString().toUtf8().length(), decrypted_arr, size_of_decrypted, message_index);

                if (length_of_decrypted == olm_error()) {
                    QString error= olm_inbound_group_session_last_error(session);
                    free(session);
                    free(decrypted_arr);
                    return bad_encrypt_str(sender, "Cannot decode: " + error);
                }

                QString decrypted_str = QString::fromUtf8((char *)decrypted_arr, length_of_decrypted);
                QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted_str.toUtf8());
                QJsonObject decrypted_obj = decrypted_json.object();
                decrypted_obj.insert("sender", sender);

                decrypted_json.setObject(decrypted_obj);

                free(session);
                free(decrypted_arr);

                return decrypted_json.toJson();
            } else {
                QString error = olm_inbound_group_session_last_error(session);
                free(session);
                return bad_encrypt_str(sender, "Unable to unpickle inbound group session: " + error);
            }
        } else {
            return bad_encrypt_str(sender, "Megolm session not found");
        }
    }

    return bad_encrypt_str(sender, "Unable to decrypt - no options: algorithm: " + algorithm);
}

void write_olm_sessions(QJsonObject sessions) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/olm_sessions.json");
    if (file.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(sessions);
        file.write(doc.toJson());
        file.close();
    }
}

void write_megolm_sessions(QJsonObject sessions) {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/megolm_sessions.json");
    if (file.open(QFile::WriteOnly)) {
        QJsonDocument doc;
        doc.setObject(sessions);
        file.write(doc.toJson());
        file.close();
    }
}

QJsonObject read_olm_sessions() {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/olm_sessions.json");
    if (file.open(QFile::ReadOnly)) {
        QJsonDocument from_file = QJsonDocument::fromJson(file.readAll());
        file.close();

        return from_file.object();
    }
    return QJsonObject();
}

QJsonObject read_megolm_sessions() {
    QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/megolm_sessions.json");
    if (file.open(QFile::ReadOnly)) {
        QJsonDocument from_file = QJsonDocument::fromJson(file.readAll());
        file.close();

        return from_file.object();
    }
    return QJsonObject();
}

bool verify_signed_json(QJsonObject json, QString expected_sender, QString expected_device_id, QString verification_key) {
    QJsonObject signatures = json.value("signatures").toObject();
    QJsonValue signatures_of_user = signatures.value(expected_sender);
    if (signatures_of_user.isUndefined()) {
        return false;
    }

    QJsonObject signatures_obj = signatures_of_user.toObject();
    int i = 0;

    OlmUtility* util = (OlmUtility*) malloc(olm_utility_size());
    if (util == nullptr) {
        qFatal("Cannot allocate memory for olm utility");
        return false;
    }

    util = olm_utility(util);

    for (QJsonValue single_sig : signatures_obj) {
        QString key = signatures_obj.keys().at(i);
        QString the_sig = single_sig.toString();
        if (key == (QString("ed25519:") + expected_device_id)) {
            QJsonDocument doc;
            doc.setObject(json);
            QString verify_against = canonical_json(doc);

            size_t verify_result = olm_ed25519_verify(util,
                                                      verification_key.toUtf8().data(),
                                                      verification_key.toUtf8().length(),
                                                      verify_against.toUtf8().data(),
                                                      verify_against.toUtf8().length(),
                                                      the_sig.toUtf8().data(),
                                                      the_sig.toUtf8().length());
            if (verify_result != olm_error()) {
                free(util);
                return true;
            } else {
                free(util);
                return false;
            }
        }
        i++;
    }
    free(util);
    return false; // Err to the side of caution
}

QString canonical_json(QJsonDocument doc) {
    QJsonObject obj = doc.object();
    obj.remove("signatures");
    obj.remove("unsigned");
    doc.setObject(obj);

    return doc.toJson(QJsonDocument::Compact);
}

void save_account(OlmAccount* account, QString user_id) {
    uint8_t* pickle_jar = (uint8_t*) malloc(olm_pickle_account_length(account));

    if (pickle_jar == nullptr) {
        qFatal("Cannot allocate memory for pickle jar");
        return;
    }

    size_t pickle_size = olm_pickle_account(account, user_id.toUtf8().data(), user_id.toUtf8().size(), pickle_jar, olm_pickle_account_length(account));

    QString pickled = QString::fromUtf8((char*) pickle_jar, pickle_size);
    QJsonDocument pickle_doc;
    QJsonObject pickle_obj;
    pickle_obj.insert("pickle", pickled);
    pickle_doc.setObject(pickle_obj);

    QFile pickle_file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/pickle-jar.json");
    if (pickle_file.open(QFile::WriteOnly)) {
        pickle_file.write(pickle_doc.toJson());
        pickle_file.close();
    } else {
        qWarning() << "Cannot save pickle jar";
    }

    free(pickle_jar);
}

QString get_olm_plaintext(QJsonObject obj, QString sender, QString recipient, QString recipient_ed25519, QString sender_ed25519) {
    obj.insert("sender", sender);
    obj.insert("recipient", recipient);
    QJsonObject recipient_keys;
    recipient_keys.insert("ed25519", recipient_ed25519);
    obj.insert("recipient_keys", recipient_keys);
    QJsonObject sender_keys;
    sender_keys.insert("ed25519", sender_ed25519);
    obj.insert("keys", sender_keys);

    QJsonDocument doc;
    doc.setObject(obj);

    return doc.toJson(QJsonDocument::Compact);
}

QString get_uuid() {
    return QUuid::createUuid().toString().remove("{").remove("}");
}
