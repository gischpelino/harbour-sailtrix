#include "messagesmodel.h"

QHash<int, QByteArray> MessagesModel::roleNames() const {
    QHash<int, QByteArray> list;
    list[user_id] = "user_id";
    list[display_name] = "display_name";
    list[avatar] = "avatar";
    list[avatar_mxc] = "avatar_mxc";
    list[content] = "content";
    list[is_self] = "is_self";
    list[event_id] = "event_id";
    list[txn_id] = "txn_id";
    list[orig_body] = "orig_body";
    list[is_deleted] = "is_deleted";
    list[is_image] = "is_image";
    list[thumbnail_url] = "thumbnail_url";
    list[img_url] = "img_url";
    list[thumbnail_path] = "thumbnail_path";
    list[image_path] = "image_path";
    list[image_width] = "image_width";
    list[image_name] = "image_name";
    list[is_notice] = "is_notice";

    return list;
}
