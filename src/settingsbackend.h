#ifndef SETTINGSBACKEND_H
#define SETTINGSBACKEND_H
#include <QObject>
#include <QNetworkReply>


class SettingsBackend : public QObject
{
    Q_OBJECT

public:
    Q_INVOKABLE void clear_cache();
    Q_INVOKABLE void clear_config();
signals:
    void done();
    void configClearDone();
private slots:
    void after_logout(QNetworkReply* reply);
};

#endif // SETTINGSBACKEND_H
