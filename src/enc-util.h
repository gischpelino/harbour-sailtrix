#ifndef ENCUTIL_H
#define ENCUTIL_H
#include <QString>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonDocument>
#include <olm/olm.h>

QString decrypt(QJsonValue encrypted, QString algorithm, QString sender_key, QString device_id, QString session_id, QString m_curve25519_id, QJsonObject* olm_sessions, QJsonObject* megolm_sessions, QString m_user_id, OlmAccount* account, QString sender, unsigned int* message_index);
void write_olm_sessions(QJsonObject sessions);
void write_megolm_sessions(QJsonObject sessions);
QJsonObject read_olm_sessions();
QJsonObject read_megolm_sessions();
bool verify_signed_json(QJsonObject json, QString expected_sender, QString expected_device_id, QString verification_key);
QString canonical_json(QJsonDocument doc);
void save_account(OlmAccount* account, QString user_id);
QString get_olm_plaintext(QJsonObject obj, QString sender, QString recipient, QString recipient_ed25519, QString sender_ed25519);
QString bad_encrypt_str(QString sender, QString reason);
bool check_olm(QString decrypted, QString sender_user_id, QString m_user_id, QString my_real_key);
bool check_olm_no_verify(QString decrypted, QString sender_user_id, QString m_user_id);
QString get_uuid();

#endif // ENCUTIL_H
