#include "rooms.h"

#include <QNetworkReply>
#include <QStandardPaths>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QStandardItem>
#include <QDir>
#include <QMimeType>
#include <QMimeDatabase>
#include <QUrl>
#include <openssl/rand.h>
#include "enc-util.h"

RoomsModel* Rooms::rooms() { return m_rooms; }

Rooms::Rooms() : m_rooms{ new RoomsModel }, m_master_obj {new QJsonObject}, olm_sessions { new QJsonObject}, megolm_sessions {new QJsonObject} , m_reply { nullptr }, message_index { 0 }, get_all { false } , load_finished { false } {};

Rooms::~Rooms() {
    delete m_rooms;
    delete m_master_obj;
    delete olm_sessions;
    delete megolm_sessions;
    delete manager;
    delete filterMaker;
    delete imageDownloader;
    delete leaver;
    delete key_uploader;
    delete message_index;
}

void Rooms::setRooms(RoomsModel *roomsModel) {
    m_rooms = roomsModel;
    emit roomsChanged();
}

bool Rooms::done() {
    return load_finished;
}

QUrl Rooms::getUrl() {
    if (!filter_id.isNull() && !filter_id.isEmpty() && !next_batch.isNull() && !next_batch.isEmpty() && !get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&since=" + next_batch + "&timeout=30000&full_state=true");
    }

    if (!filter_id.isNull() && !filter_id.isEmpty() && !get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&timeout=30000&full_state=true");
    }

    if (!filter_id.isNull() && !filter_id.isEmpty() && !next_batch.isNull() && !next_batch.isEmpty() && get_all) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter=" + filter_id + "&since=" + next_batch + "&full_state=true");
    }

    qDebug() << "Not Using Filter";
    if (next_batch.isNull() || next_batch.isEmpty()) {
        return QUrl(hs_url + "/_matrix/client/r0/sync?filter={ \"account_data\": { \"types\" : [\"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": { \"types\": [\"m.room.name\", \"m.room.avatar\", \"m.room.encrypted\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"types\":[\"m.room.message\"],\"limit\":1}}}&timeout=30000&full_state=true");
    }
    return QUrl(hs_url + "/_matrix/client/r0/sync?filter={ \"account_data\": { \"types\" : [ \"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": { \"types\": [\"m.room.name\", \"m.room.avatar\", \"m.room.encrypted\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"types\":[\"m.room.message\"],\"limit\":1}}}&since=" + next_batch + "&timeout=30000&full_state=true");
}

QString Rooms::get_filter_json() {
    return QString("{ \"account_data\": { \"types\" : [\"m.direct\",\"m.ignored_user_list\"] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": { \"types\": [\"m.room.name\", \"m.room.avatar\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"types\":[\"m.room.message\"],\"limit\":1}}}");
}

void Rooms::write_cache() {
    QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/matrix-rooms.cache.json");
    if (cache_file.open(QFile::WriteOnly)) {
        QJsonDocument master_doc;
        master_doc.setObject(*m_master_obj);
        cache_file.write(master_doc.toJson());
        cache_file.close();
    }
}

void Rooms::maintain_key_count() {
    int needed = (olm_account_max_number_of_one_time_keys(account) / 2) - num_keys_on_server;
    qDebug() << needed << " keys needed.";

    if (needed <= 0) {
        qDebug() << "Returning";
        return;
    }

    unsigned char* random_for_otk = (unsigned char*) std::malloc(olm_account_generate_one_time_keys_random_length(account, needed));

    RAND_bytes(random_for_otk, olm_account_generate_one_time_keys_random_length(account, needed));

    size_t error = olm_account_generate_one_time_keys(account, needed, random_for_otk, olm_account_generate_one_time_keys_random_length(account, needed));

    if (error != olm_error()) {
        uint8_t* otk_arr = (uint8_t*) malloc(olm_account_one_time_keys_length(account));
        size_t otk_json_len = olm_account_one_time_keys(account, otk_arr, olm_account_one_time_keys_length(account));
        QString otk_str = QString::fromUtf8((char*) otk_arr, otk_json_len);
        qDebug() << otk_str;

        QJsonObject one_time_keys_obj;
        QJsonDocument otk_doc = QJsonDocument::fromJson(otk_str.toUtf8());

        int k = 0;
        auto otk_ids = otk_doc.object().value("curve25519").toObject().keys();
        for (QJsonValue value : otk_doc.object().value("curve25519").toObject()) {
            QString key_id = otk_ids.at(k);
            QString the_key = value.toString();

            QJsonDocument to_sign;
            QJsonObject sign_obj;
            sign_obj.insert("key", the_key);
            to_sign.setObject(sign_obj);

            QString sign_message = to_sign.toJson(QJsonDocument::Compact);

            uint8_t* signed_key = (uint8_t*) std::malloc(olm_account_signature_length(account));
            size_t signed_key_length = olm_account_sign(account, (unsigned char*) sign_message.toUtf8().data(), sign_message.length(), signed_key, olm_account_signature_length(account));

            QJsonObject new_obj;
            new_obj.insert("key", the_key);

            QJsonObject signatures_obj;
            QJsonObject signature_obj;

            signature_obj.insert("ed25519:" + device_id, QString::fromUtf8((char*) signed_key, signed_key_length));
            signatures_obj.insert(user_id, signature_obj);

            new_obj.insert("signatures", signatures_obj);
            one_time_keys_obj.insert("signed_curve25519:" + key_id, new_obj);

            free(signed_key);
            k++;
        }

        QNetworkRequest req(hs_url + "/_matrix/client/r0/keys/upload");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
        req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

        QJsonObject master_obj;
        master_obj.insert("one_time_keys", one_time_keys_obj);
        QJsonDocument upload_doc;
        upload_doc.setObject(master_obj);

        key_uploader->post(req, upload_doc.toJson());
        save_account(account, user_id);

    } else {
        qWarning() << olm_account_last_error(account);
    }

    free(random_for_otk);
}

void Rooms::trigger_filter() {
    filterMaker = new QNetworkAccessManager(this);
    connect(filterMaker, &QNetworkAccessManager::finished, this, &Rooms::handleFilter);

    QNetworkRequest makeFilterRequest(hs_url + "/_matrix/client/r0/user/" + user_id + "/filter");
    makeFilterRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
    makeFilterRequest.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");

    filterMaker->post(makeFilterRequest, get_filter_json().toUtf8());
}

bool Rooms::load() {
    QFile config (QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/sailtrix.config.json");
    if (config.open(QFile::ReadOnly)) {
        QJsonDocument document = QJsonDocument::fromJson(config.readAll());

        if (document.object().value("home_server").toString() == ""
                || document.object().value("access_token").toString() == "") return false;

        manager = new QNetworkAccessManager(this);
        imageDownloader = new QNetworkAccessManager(this);
        leaver = new QNetworkAccessManager(this);
        key_uploader = new QNetworkAccessManager(this);

        connect(manager, &QNetworkAccessManager::finished, this, &Rooms::handleRooms);
        connect(key_uploader, &QNetworkAccessManager::finished, this, &Rooms::uploadKeysDone);

        hs_url = document.object().value("home_server").toString();
        access_token = document.object().value("access_token").toString();
        user_id = document.object().value("user_id").toString();
        device_id = document.object().value("device_id").toString();

        QFile pickle_file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/pickle-jar.json");
        if (pickle_file.open(QFile::ReadOnly)) {
            QJsonDocument pickle_doc = QJsonDocument::fromJson(pickle_file.readAll());
            QString pickle_str = pickle_doc.object().value("pickle").toString();
            qDebug() << pickle_str;

            account = (OlmAccount*) malloc(olm_account_size());
            account = olm_account(account);
            olm_unpickle_account(account, user_id.toUtf8().data(), user_id.toUtf8().size(), pickle_str.toUtf8().data(), pickle_str.toUtf8().size());
            qDebug() << olm_account_last_error(account);
            pickle_file.close();
        }

        *olm_sessions = read_olm_sessions();
        *megolm_sessions = read_megolm_sessions();

        QFile encryption_keys(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/identity_keys.json");
        if (encryption_keys.open(QFile::ReadOnly)) {
            QJsonDocument ek_doc = QJsonDocument::fromJson(encryption_keys.readAll());
            m_curve25519_id = ek_doc.object().value("curve25519").toString();
            encryption_keys.close();
        }

        QFile cache_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/matrix-rooms.cache.json");
        if (cache_file.exists() && cache_file.open(QFile::ReadWrite)) {
            QJsonDocument cache_json = QJsonDocument::fromJson(cache_file.readAll());

            loadCache(cache_json);
            next_batch = cache_json.object().value("next_batch").toString();

            if (!next_batch.isNull() && !next_batch.isEmpty()) {
                load_finished = true;
                emit done_chg();
            }

            filter_id = cache_json.object().value("filter_id").toString();

            if (filter_id.isNull() || filter_id.isEmpty()) {
                // Trigger the filter creator
                trigger_filter();
            }

            QNetworkRequest nextRequest(getUrl());
            nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

            m_reply = manager->get(nextRequest);
            *m_master_obj = cache_json.object();

            cache_file.close();
        } else {
            qDebug() << "Getting all data...";

            QNetworkRequest reply(QUrl(document.object().value("home_server").toString() + "/_matrix/client/r0/sync?filter={ \"account_data\": { \"types\" : [] }, \"room\": { \"account_data\": { \"types\" : []}, \"state\": { \"types\": [\"m.room.name\", \"m.room.avatar\"] , \"limit\" : 1}, \"ephemeral\": { \"types\": [], \"limit\" : 1}, \"timeline\":{\"types\":[\"m.room.message\"],\"limit\":1}}}"));
            reply.setRawHeader(QByteArray("Authorization"), (QString("Bearer ") + document.object().value("access_token").toString()).toUtf8());

            m_reply = manager->get(reply);

            trigger_filter();

            is_loaded = true;

        }



        connect(imageDownloader, &QNetworkAccessManager::finished, this, &Rooms::downloadRoomAvatar);
        is_loaded = true;


        return true;
    }

    is_loaded = true;

    return true;
}

void Rooms::handleRooms(QNetworkReply* reply) {
    get_all = false;

    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << "sync error: " << reply->errorString();
        return;
    }

    m_master_obj = new QJsonObject();
    qDebug() << "Processing" << reply->url();

    QJsonDocument replyDoc = QJsonDocument::fromJson(reply->readAll());
    QJsonObject replyObj = replyDoc.object();
    QJsonObject rooms = replyObj.value("rooms").toObject();
    QJsonObject joinedRooms = rooms.value("join").toObject();

    next_batch = replyObj.value("next_batch").toString();

    QDir cache_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

    if (!cache_dir.exists()) {
        cache_dir.mkpath(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    }

    // save DMs

    QJsonArray dms = replyObj.value("account_data").toObject().value("events").toArray();
    QJsonObject dm_obj;

    for (QJsonValue value : dms) {
        QJsonObject val = value.toObject();

        if (val.value("type") == QString("m.direct")) {
            dm_obj = val.value("content").toObject();
            break;
        }
    }

    if (!dm_obj.isEmpty()) {
        QFile direct_file(QStandardPaths::writableLocation(QStandardPaths::CacheLocation) + "/direct.json");
        if (direct_file.open(QFile::WriteOnly)) {
            direct_file.write(QJsonDocument(dm_obj).toJson());
            direct_file.close();
        }
    }

    QJsonObject rooms_cache;
    QJsonArray to_Device_events = replyObj.value("to_device")
            .toObject()
            .value("events")
            .toArray();
    qDebug() << "NUMBER OF TO-DEVICE EVENTS: " << to_Device_events.size();

    qDebug() << to_Device_events;
    for (QJsonValue event : to_Device_events) {
        QJsonObject event_obj = event.toObject();
        QJsonObject content = event_obj.value("content").toObject();

        QString type = event_obj.value("type").toString();
        qDebug() << "DECRYPTING " << type;

        if (type == QString("m.room.encrypted")) {
            QString decrypted = decrypt(content.value("ciphertext"),
                                        content.value("algorithm").toString(),
                                        content.value("sender_key").toString(),
                                        content.value("device_id").toString(),
                                        content.value("session_id").toString(),
                                        m_curve25519_id,
                                        olm_sessions,
                                        megolm_sessions,
                                        user_id,
                                        account,
                                        event_obj.value("sender").toString(),
                                        message_index);

            write_olm_sessions(*olm_sessions);

            if (!check_olm_no_verify(decrypted,
                                     event_obj.value("sender").toString(),
                                     user_id)) {
                qWarning() << "Olm plaintext properties mismatch!";
                continue;
            }

            QJsonDocument decrypted_json = QJsonDocument::fromJson(decrypted.toUtf8());
            QJsonObject decrypted_obj = decrypted_json.object();

            if (decrypted_obj.value("type").toString() == QString("m.room_key")) {
                QJsonObject room_key_content = decrypted_obj.value("content").toObject();

                if (!megolm_sessions->contains(room_key_content.value("session_id").toString())) {
                    OlmInboundGroupSession* session = (OlmInboundGroupSession*) malloc(olm_inbound_group_session_size());
                    session = olm_inbound_group_session(session);

                    size_t result = olm_init_inbound_group_session(session, (unsigned char*) room_key_content.value("session_key").toString().toUtf8().data(), room_key_content.value("session_key").toString().toUtf8().length());
                    if (result != olm_error()) {
                        char* pickle_igs = (char*) malloc(olm_pickle_inbound_group_session_length(session));
                        size_t pickle_length = olm_pickle_inbound_group_session(session, user_id.toUtf8().data(), user_id.toUtf8().length(), pickle_igs, olm_pickle_inbound_group_session_length(session));

                        if (pickle_length != olm_error()) {
                            QJsonObject obj = room_key_content;
                            obj.insert("pickle", QString::fromUtf8(pickle_igs, pickle_length));
                            megolm_sessions->insert(room_key_content.value("session_id").toString(), obj);

                            write_megolm_sessions(*megolm_sessions);

                        } else {
                            qWarning() << "failed to pickle group session:" << olm_inbound_group_session_last_error(session);
                        }
                    } else {
                        qWarning() << "Olm init inbound group session error: " << olm_inbound_group_session_last_error(session);
                    }

                }
            }

        }
    }

    int i = 0;
    for (QJsonValue obj : joinedRooms) {
        QJsonObject individualRoom = obj.toObject();
        QJsonArray events = individualRoom.value("state")
                .toObject()
                .value("events")
                .toArray();

        QString room_name;
        QString cached_mid = m_rooms->data(m_rooms->index(i,0), RoomsModel::mxcUrl).toString();
        QString avatar_mxc_url = cached_mid;
        bool found = false;
        bool has_name = false;

        for (QJsonValue event : events) {
            QJsonObject obj = event.toObject();
            if (obj.value("type").toString() == "m.room.name") {
                room_name = obj.value("content")
                        .toObject()
                        .value("name")
                        .toString();

                has_name = true;

            }
            else if (obj.value("type").toString() == "m.room.avatar") {
                avatar_mxc_url = obj.value("content")
                        .toObject()
                        .value("url")
                        .toString();

                found = true;
            }
        }

        QString room_id = joinedRooms.keys().at(i);


        QJsonArray heroes = individualRoom.value("summary").toObject().value("m.room.heroes").toArray();

        if (!heroes.isEmpty() && !has_name) {
            if (heroes.size() > 1) {
                room_name = heroes.at(0).toString() + " and " + (heroes.size()-1) + " more";
            } else {
                room_name = heroes.at(0).toString();
            }
            has_name = true;
        }

        if (!has_name) {
            int j = 0;
            for (QJsonValue value : dm_obj) {
                QJsonArray arr = value.toArray();
                QString user_id = dm_obj.keys().at(j);

                if (arr.contains(room_id)) {
                    room_name = user_id;
                    qDebug() << "found DM";

                    if (!found) {
                        avatar_mxc_url = "image://theme/icon-m-users";
                        found = true;
                    }

                    has_name = true;
                    break;
                }
                j++;
            }

        }

        if (!has_name) {
            room_name = joinedRooms.keys().at(i);
        }

        if (!found) {
            avatar_mxc_url = "image://theme/icon-m-chat";
        }


        QJsonObject latestMessage = individualRoom.value("timeline")
                .toObject()
                .value("events")
                .toArray()
                .at(0)
                .toObject();

        QString lastest_msg_text("");
        if (latestMessage.value("content").toObject().value("msgtype") == "m.text") {
            lastest_msg_text = latestMessage.value("content").toObject().value("body").toString();
        }

        lastest_msg_text.replace("\n", " / ");

        QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
        QString media_id = QUrl(avatar_mxc_url).path();

        media_id.remove(0,1);
        int unreadNotificationsCount = individualRoom.value("unread_notifications")
                .toObject()
                .value("notification_count")
                .toInt();

        bool isIn = false;



        for (int j = 0; j < m_rooms->rowCount(); j++) {
            QString orig_name = m_rooms->data(m_rooms->index(j,0), RoomsModel::name).toString();
            QString orig_id = m_rooms->data(m_rooms->index(j,0), RoomsModel::roomId).toString();

            if (orig_id == room_id) {
                isIn = true;
                QString orig_latestMsg = m_rooms->data(m_rooms->index(j,0), RoomsModel::firstMessage).toString();
                QString orig_mediaId = m_rooms->data(m_rooms->index(j,0), RoomsModel::mxcUrl).toString();

                m_rooms->setData(m_rooms->index(j, 0), room_name.isEmpty() ? orig_name : room_name, RoomsModel::name);
                m_rooms->setData(m_rooms->index(j, 0), lastest_msg_text.isEmpty() ? orig_latestMsg : lastest_msg_text, RoomsModel::firstMessage);
                m_rooms->setData(m_rooms->index(j, 0), media_id.isEmpty() ? orig_mediaId : avatar_mxc_url, RoomsModel::mxcUrl);
                m_rooms->setData(m_rooms->index(j, 0), unreadNotificationsCount, RoomsModel::unreadMessages);

            }
        }

        if (!isIn) {
            QStandardItem* item = new QStandardItem();
            item->setData(room_name, RoomsModel::name);
            item->setData(lastest_msg_text, RoomsModel::firstMessage);
            item->setData(avatar_mxc_url, RoomsModel::mxcUrl);

            item->setData(room_id, RoomsModel::roomId);
            item->setData(unreadNotificationsCount, RoomsModel::unreadMessages);
            // set this later item->setData(cache.absolutePath() + "/" + media_id + ".png", RoomsModel::icon);
            m_rooms->appendRow(item);
        }

        // Download avatar - don't want to slow down roomsChanged event
        if (avatar_mxc_url != "" && avatar_mxc_url != "image://theme/icon-m-chat" && avatar_mxc_url != "image://theme/icon-m-users") {
            QString server_name = QUrl(avatar_mxc_url).host();
            // see if cached...

            if (cache_dir.exists() && cache_dir.exists(media_id + ".png")) {
                m_rooms->setData(m_rooms->index(i, 0), cache.absolutePath() + "/" + media_id + ".png", RoomsModel::icon);
            } else {
                imageDownloader->get(QNetworkRequest(hs_url + QString("/_matrix/media/r0/download/" + server_name + "/" + media_id)));
            }
        }

        emit roomsChanged();


        if (!m_master_obj->keys().contains("rooms_list")) {
            m_master_obj->insert("rooms_list", QJsonObject());
        }

        QJsonObject thisRoom;

        thisRoom.insert("id", m_rooms->data(m_rooms->index(i,0), RoomsModel::roomId).toString());
        thisRoom.insert("name", m_rooms->data(m_rooms->index(i,0), RoomsModel::name).toString());
        thisRoom.insert("latest_message", m_rooms->data(m_rooms->index(i,0), RoomsModel::firstMessage).toString());
        thisRoom.insert("mxc_url", m_rooms->data(m_rooms->index(i,0), RoomsModel::mxcUrl).toString());
        thisRoom.insert("unread_messages", m_rooms->data(m_rooms->index(i,0), RoomsModel::unreadMessages).toString());
        thisRoom.insert("avatar", m_rooms->data(m_rooms->index(i,0), RoomsModel::icon).toString());



        rooms_cache.insert(m_rooms->data(m_rooms->index(i,0), RoomsModel::roomId).toString(), thisRoom);
        i++;

    }



    m_master_obj->insert("rooms_list", QJsonValue(rooms_cache));

    m_master_obj->insert("next_batch", next_batch);
    m_master_obj->insert("filter_id", filter_id);

    load_finished = true;
    emit done_chg();

    write_cache();

    QJsonObject otk_count = replyObj.value("device_one_time_keys_count").toObject();
    num_keys_on_server = otk_count.value("signed_curve25519").toInt();

    maintain_key_count();

    if (!is_paused) {
        qDebug() << "Starting next batch";
        QNetworkRequest nextRequest(getUrl());
        nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        m_reply = manager->get(nextRequest);
    }
    qDebug() << "Here";
}

void Rooms::downloadRoomAvatar(QNetworkReply* reply) {
    if (reply->error() != QNetworkReply::NoError) {
        qWarning() << reply->errorString() << reply->readAll();
        return;
    }
    qDebug() << "Downloading Avatar";
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    if (!cache.exists()) cache.mkpath(cache.absolutePath());

    QMimeDatabase db;
    QMimeType type = db.mimeTypeForName(reply->rawHeader("Content-Type"));

    QFile avatar(cache.absolutePath() + "/" + reply->url().fileName() + "." + type.preferredSuffix());

    if (avatar.open(QFile::WriteOnly)) {

        avatar.write(reply->readAll());
        avatar.close();


        for (int i = 0; i < m_rooms->rowCount(); i++) {
            QString media_id = QUrl(m_rooms->data(m_rooms->index(i, 0), RoomsModel::mxcUrl).toString()).path();
            media_id.remove(0,1);

            if (media_id == "") {
                continue;
            }


            if (media_id == reply->url().fileName()) {
                QString path = cache.absolutePath() + "/" + reply->url().fileName() + "." + type.preferredSuffix();

                m_rooms->setData(m_rooms->index(i, 0), path, RoomsModel::icon);
                qDebug() << "Setting icon of " << m_rooms->data(m_rooms->index(i, 0), RoomsModel::name).toString() << "to " <<path << "orig mediaId:" << media_id;

                emit roomsChanged();

                break;
            }
        }
    }
    qDebug() << "Wrote avatar";

    emit avatarChanged();
}

void Rooms::loadCache(QJsonDocument doc) {
    QJsonObject mainObj = doc.object();
    for (QJsonValue room : mainObj.value("rooms_list").toObject()) {
        QJsonObject thisRoom = room.toObject();

        QStandardItem* item = new QStandardItem();
        item->setData(thisRoom.value("name").toString(), RoomsModel::name);
        item->setData(thisRoom.value("latest_message").toString(), RoomsModel::firstMessage);
        item->setData(thisRoom.value("mxc_url").toString(), RoomsModel::mxcUrl);

        QDir cache_dir(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));

        if (thisRoom.value("mxc_url").toString() != "") {
            QString media_id = QUrl(thisRoom.value("mxc_url").toString()).path();
            media_id.remove(0, 1);
            if (cache_dir.exists() && cache_dir.exists(media_id + ".png")) {
                item->setData(cache_dir.absolutePath() + "/" + media_id + ".png", RoomsModel::icon);
            }
        } else {
            item->setData(thisRoom.value("avatar").toString(), RoomsModel::icon);
        }

        item->setData(thisRoom.value("id").toString(), RoomsModel::roomId);
        item->setData(thisRoom.value("unread_messages").toString(), RoomsModel::unreadMessages);

        m_rooms->appendRow(item);
    }

    *m_master_obj = QJsonObject(mainObj);
    emit roomsChanged();
}

void Rooms::handleFilter(QNetworkReply* reply) {
    qDebug() << "Processing new filter" << reply->errorString();
    if (reply->error() == QNetworkReply::NoError) {
        qDebug() << "Filter Created";
        QJsonDocument reply_doc = QJsonDocument::fromJson(reply->readAll());
        filter_id = reply_doc.object().value("filter_id").toString();
    } else {
        qDebug() << reply->readAll();
    }
}

void Rooms::leave(QString room_id) {
    QNetworkRequest req(hs_url + "/_matrix/client/r0/rooms/" + room_id + "/leave");
    req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));
    leaver->post(req, "");

    int row_id = -1;

    for (int i = 0; i < m_rooms->rowCount(); i++) {
        if (m_rooms->data(m_rooms->index(i, 0), RoomsModel::roomId).toString() == room_id) {
            row_id = i;
        }
    }

    if (row_id != -1) {
        m_rooms->removeRow(row_id);
        qDebug() << row_id << "Removed.";
    }
}

void Rooms::uploadKeysDone(QNetworkReply* reply) {
    if (reply->error() == QNetworkReply::NoError) {
        olm_account_mark_keys_as_published(account);
        save_account(account, user_id);

        QJsonDocument doc = QJsonDocument::fromJson(reply->readAll());
        num_keys_on_server = doc.object().value("one_time_key_counts").toObject().value("signed_curve25519").toInt();
    } else {
        qDebug() << reply->readAll();
    }
}

void Rooms::pause() {
    is_paused = true;
    load_finished = false;
    m_reply->abort();
}

void Rooms::resume() {
    is_paused = false;

    *olm_sessions = read_olm_sessions();
    *megolm_sessions = read_megolm_sessions();

    get_all = true;

    qDebug() << "Continuing";
    QNetworkRequest nextRequest(getUrl());
    nextRequest.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

    m_reply = manager->get(nextRequest);
}

bool Rooms::loaded() {
    return is_loaded;
}

