#include "settingsbackend.h"
#include <QDir>
#include <QStandardPaths>
#include <QNetworkAccessManager>
#include <QJsonDocument>
#include <QJsonObject>

void SettingsBackend::clear_cache() {
    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    cache.removeRecursively();
    emit done();
}

void SettingsBackend::clear_config() {
    QNetworkAccessManager *manager = new QNetworkAccessManager();
    connect(manager, &QNetworkAccessManager::finished, this, &SettingsBackend::after_logout);

    QFile conf_file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/sailtrix.config.json");
    if (conf_file.open(QFile::ReadOnly)) {
        QJsonDocument document = QJsonDocument::fromJson(conf_file.readAll());
        QString access_token = document.object().value("access_token").toString();
        QString hs_url = document.object().value("home_server").toString();

        QNetworkRequest req(hs_url + "/_matrix/client/r0/logout");
        req.setRawHeader(QByteArray("Authorization"), (QString("Bearer " + access_token).toUtf8()));

        manager->post(req, "");
    }
    QDir config(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation));
    config.removeRecursively();

    QDir cache(QStandardPaths::writableLocation(QStandardPaths::CacheLocation));
    cache.removeRecursively();
}

void SettingsBackend::after_logout(QNetworkReply* reply) {
    qDebug() << "Here";
    emit configClearDone();
}
