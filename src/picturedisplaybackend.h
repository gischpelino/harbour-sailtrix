#ifndef PICTUREDISPLAYBACKEND_H
#define PICTUREDISPLAYBACKEND_H

#include <QObject>
#include <QNetworkAccessManager>

class PictureDisplayBackend : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString path READ path NOTIFY loaded)
public:
    explicit PictureDisplayBackend(QObject *parent = nullptr);
    ~PictureDisplayBackend();
    Q_INVOKABLE void load(QString mxc);
    QString path();
signals:
    void loaded();
private:
    QNetworkAccessManager* manager;
    QString p;
private slots:
    void process(QNetworkReply* reply);
};

#endif // PICTUREDISPLAYBACKEND_H
