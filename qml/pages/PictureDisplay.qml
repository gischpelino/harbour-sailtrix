import QtQuick 2.0
import Sailfish.Silica 1.0
import PictureDisplayBackend 1.0

Page {
    id: page

    property string image_name;
    property string mxc;

    allowedOrientations: Orientation.All

    PictureDisplayBackend { id: backend }

    Column {
        width: page.width
        PageHeader {
            title: image_name
        }

        AnimatedImage {
            source: backend.path
            width: parent.width
            fillMode: Image.PreserveAspectFit
        }
    }

    PageBusyIndicator {
        running: !backend.path
    }

    Component.onCompleted: {
        backend.load(mxc);
    }
}
